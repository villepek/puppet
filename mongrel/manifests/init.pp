# Install Ruby Mongrel packages.
#
class mongrel {

    require ruby::rubygems

    case $::operatingsystem {
        "centos","redhat": {
            case $::operatingsystemrelease {
                /^[1-5]/: {
                    $mongrel_package = "rubygem-mongrel"
                    $mongrel_provider = undef
                }
                default: {
                    $mongrel_package = "mongrel"
                    $mongrel_provider = "gem"
                }
            }
        }
        "debian","ubuntu": {
            $mongrel_package = "mongrel"
            $mongrel_provider = undef
        }
        "openbsd": {
            $mongrel_package = "ruby-mongrel"
            $mongrel_provider = undef
        }
        default: {
            $mongrel_package = "rubygem-mongrel"
            $mongrel_provider = undef
        }
    }

    package { "mongrel":
        ensure   => installed,
        name     => $mongrel_package,
        provider => $mongrel_provider,
    }

}
