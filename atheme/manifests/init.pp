# Use default values from charybdis if defined.
#
class atheme::params {

  if $::charybdis::services_name {
    $services_name = $::charybdis::services_name
  } else {
    $services_name = 'ircservices.localdomain'
  }

  if $::charybdis::network_name {
    $network_name = $::charybdis::network_name
  } else {
    $network_name = 'IRC Network'
  }

  if $::charybdis::admin_name {
    $admin_name = $::charybdis::admin_name
  } else {
    $admin_name = 'Administrator'
  }

  if $::charybdis::admin_email {
    $admin_email = $::charybdis::admin_email
  } else {
    $admin_email = 'root@localhost'
  }

  if $::charybdis::server_name {
    $uplink_name = $::charybdis::server_name
  } else {
    $uplink_name = undef
  }

  if $::charybdis::port {
    $uplink_port = $::charybdis::port
  } else {
    $uplink_port = '6667'
  }

  if $::charybdis::services_password {
    $uplink_password = $::charybdis::services_password
  } else {
    $uplink_password = undef
  }

}

# Install Atheme IRC services.
#
class atheme(
  $sid='00B',
  $services_name=$atheme::params::services_name,
  $services_description='Atheme IRC Services',
  $network_name=$atheme::params::network_name,
  $admin_name=$atheme::params::admin_name,
  $admin_email=$atheme::params::admin_email,
  $hidehostsuffix='hiddendomain',
  $uplink_name=$atheme::params::uplink_name,
  $uplink_port=$atheme::params::uplink_port,
  $uplink_password=$atheme::params::uplink_password,
  $operators=[],
) inherits atheme::params {

  case $::operatingsystem {
    'ubuntu': { }
    default: {
      fail("atheme not supported on ${::operatingsystem}")
    }
  }

  if ! $uplink_name {
    fail('Must define $uplink_name')
  }

  if ! $uplink_password {
    fail('Must define $uplink_password')
  }

  package { 'atheme-services':
    ensure => installed,
  }

  file { '/etc/atheme/atheme.conf':
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('atheme/atheme.conf.erb'),
    require => Package['atheme-services'],
    notify  => Service['atheme-services'],
  }

  augeas { 'atheme-enable':
    context => '/files/etc/default/atheme-services',
    changes => 'set ENABLED 1',
    notify  => Service['atheme-services'],
  }

  service { 'atheme-services':
    ensure    => running,
    enable    => true,
    hasstatus => false,
  }

  file { '/var/log/atheme':
    ensure  => directory,
    mode    => '0640',
    owner   => 'irc',
    group   => 'irc',
    recurse => true,
    require => Service['atheme-services'],
  }

  if defined(Service['charybdis']) {
    Service['atheme-services'] {
      require => Service['charybdis'],
    }
  }

}
