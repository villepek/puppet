
# Install Lm_Sensors
#
class lm_sensors {

    package { "lm_sensors":
        ensure => installed,
        notify => Exec["sensors-detect"],
    }

    exec { "sensors-detect":
        command     => "cat /dev/null | sensors-detect",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
        before      => Service["lm_sensors"],
    }

    service { "lm_sensors":
        ensure    => running,
        hasstatus => true,
        enable    => true,
    }

}
