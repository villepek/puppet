# Install Redis.
#
# === Parameters
#
# $bind:
#   Address to bind to. Defaults to '127.0.0.1'.
#
# $datadir:
#   Directory for redis database dumps.
#   Defaults to '/srv/redis'.
#
# $password:
#   Optional password for client connections.
#
# $appendonly:
#   Enable append only mode. Defaults to false.
#
class redis(
  $bind='127.0.0.1',
  $datadir='/srv/redis',
  $password=undef,
  $appendonly=false,
) {

  package { 'redis':
    ensure => installed,
  }

  if $datadir != '/srv/redis' {
    file { '/srv/redis':
      ensure => link,
      target => $datadir,
      before => Service['redis'],
    }
  }

  file { $datadir:
    ensure  => directory,
    mode    => '0700',
    owner   => 'redis',
    group   => 'redis',
    before  => Service['redis'],
    require => Package['redis'],
  }

  augeas { 'set-redis-include':
    changes => 'set include /etc/redis.local.conf',
    incl    => '/etc/redis.conf',
    lens    => 'Spacevars.simple_lns',
    notify  => Service['redis'],
    require => Package['redis'],
  }

  file { '/etc/redis.local.conf':
    ensure  => present,
    mode    => '0600',
    owner   => 'redis',
    group   => 'redis',
    content => template('redis/local.conf.erb'),
    notify  => Service['redis'],
    require => Package['redis'],
  }

  service { 'redis':
    ensure => running,
    enable => true,
  }

}
