# Install tmux.
#
# === Parameters
#
# $source:
#   Configuration file source. None by default.
#
class tmux($source=undef) {

  if $::operatingsystem != 'OpenBSD' {
    package { 'tmux':
      ensure => installed,
      before => $source ? {
        undef   => undef,
        default => File['/etc/tmux.conf'],
      },
    }
  }

  if $source {
    file { '/etc/tmux.conf':
      ensure => present,
      mode   => '0644',
      owner  => 'root',
      group  => $::operatingsystem ? {
        'openbsd' => 'wheel',
        default   => 'root',
      },
      source => $source,
    }
  }

}
