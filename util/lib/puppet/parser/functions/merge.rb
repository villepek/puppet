module Puppet::Parser::Functions
    newfunction(:merge, :type => :rvalue) do |args|

        if args.length < 2
            raise Puppet::ParseError, ("merge(): wrong number of arguments (#{args.length}; must be at least 2)")
        end

        ret = []
        args.each do |arg|
            next if arg == ""
            if arg.is_a?(Array)
                ret.concat(arg)
            else
                ret.concat([arg])
            end
        end
        ret

    end
end
