
Facter.add("apparmor") do
    setcode do
        result = false
        if File.exists?("/sys/module/apparmor")
            begin
                f = File.new("/proc/mounts")
                while (line = f.gets)
                    if line.split()[2] == "securityfs"
                        if File.exists?(File.join(line.split()[1], "apparmor"))
                            result = true
                        end
                    end
                end
            rescue
            end
        end
        result
    end
end
