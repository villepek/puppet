
# Install APC UPS control (apcupsd).
#
class apcupsd {

    package { "apcupsd":
        ensure => installed,
    }

    service { "apcupsd":
        ensure  => running,
        enable  => true,
        require => Package["apcupsd"],
    }

    file { "/etc/apcupsd/apcupsd.conf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["apcupsd"],
        notify  => Service["apcupsd"],
    }

}

# Install APC UPS client.
#
# === Global variables
#
#   $apcupsd_server:
#       Server address where apcupsd server is running.
#
class apcupsd::client inherits apcupsd {

    File["/etc/apcupsd/apcupsd.conf"] {
        content => template("apcupsd/apcupsd.conf.client.erb"),
    }

}
