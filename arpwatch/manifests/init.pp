
# Install arpwatch daemon
#
# === Parameters
#
#     $interface:
#         Network interface to monitor.
#
class arpwatch($interface) {

    package { "arpwatch":
        ensure => installed,
    }

    file { "/etc/sysconfig/arpwatch":
        ensure  => present,
        content => template("arpwatch/arpwatch.sysconfig.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["arpwatch"],
        notify  => Service["arpwatch"],
    }

    service { "arpwatch":
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package["arpwatch"],
    }

}
