# Install VMware optimizations if running as guest.
#
class vmware::guest {

    case $::virtual {
        "vmware": {
            case $::operatingsystem {
                "centos": { include vmware::guest::centos }
                "ubuntu": { include vmware::guest::ubuntu }
            }
        }
    }

}


# Handler for CentOS.
#
class vmware::guest::centos {

    yum::repo { "vmware-tools":
        descr   => "VMware Tools Repository",
        baseurl => "http://packages.vmware.com/tools/esx/5.0/rhel\$releasever/\$basearch",
    }

    package { "VMwareTools":
        ensure => absent,
    }

    package { [ "vmware-tools-esx-nox",
                "vmware-tools-esx-kmods", ]:
        ensure  => installed,
        require => [ Package["VMwareTools"],
                     Yum::Repo["vmware-tools"], ],
    }

}


# Handler for Ubuntu.
#
class vmware::guest::ubuntu {

    require gnu::gcc
    require gnu::make

    if ! $vmware_tools_package {
        if $vmware_tools_package_latest {
            $vmware_tools_package = $vmware_tools_package_latest
        } else {
            fail("Must define \$vmware_tools_package or \$vmware_tools_package_latest")
        }
    }

    file { "/usr/local/src/vmwaretools.tar.gz":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => "puppet:///files/packages/${vmware_tools_package}",
    }
    util::extract::tar { "/usr/local/src/vmwaretools":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/vmwaretools.tar.gz",
        require => File["/usr/local/src/vmwaretools.tar.gz"],
    }

    if versioncmp($::kernelversion, "3.9") >= 0 {
        $creates = "/etc/init/vmware-tools.conf"
    } else {
        $creates = "/lib/modules/${::kernelrelease}/misc/vmci.ko"
    }

    exec { "vmware-install.pl":
        command     => "/bin/sh -c 'umask 022; unset DISPLAY REMOTEHOST SSH_CONNECTION; perl vmware-install.pl -d; test -f ${creates}'",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        cwd         => "/usr/local/src/vmwaretools",
        environment => [ "PAGER=/bin/cat", ],
        creates     => $creates,
        require     => Util::Extract::Tar["/usr/local/src/vmwaretools"],
    }

    file { "/etc/init.d/vmware-tools":
        ensure => link,
        target => "/lib/init/upstart-job",
        before => Service["vmware-tools"],
    }
    service { "vmware-tools":
        ensure    => running,
        enable    => true,
        subscribe => Exec["vmware-install.pl"],
    }

}
