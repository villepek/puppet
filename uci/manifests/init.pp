
# Helper class for UCI commit
#
class uci {

    if $::operatingsystem != "OpenWRT" {
        fail("uci not supported on ${::operatingsystem}")
    }

    exec { "uci commit":
        user        => "root",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
    }

}

# Set value using UCI
#
# === Parameters
#
#     $name:
#         UCI key to set
#     $value:
#         Value for given key
#
# === Sample usage
#
# uci::set { "uhttpd.main.listen_http":
#     value => "8080",
# }
#
define uci::set($value) {

    include uci

    exec { "uci set ${name}=${value}":
        user   => "root",
        path   => "/bin:/usr/bin:/sbin:/usr/sbin",
        unless => "uci get ${name} | egrep '^${value}\$'",
        notify => Exec["uci commit"],
    }

}
