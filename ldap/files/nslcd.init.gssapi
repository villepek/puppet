#!/bin/sh
#
# chkconfig: - 12 88 
# description: Provides naming services using a directory server.
# processname: /usr/sbin/nslcd
# config: /etc/nslcd.conf
# pidfile: /var/run/nslcd/nslcd.pid
#

### BEGIN INIT INFO
# Provides: nslcd
# Required-Start: $network
# Required-Stop:
# Default-Start:
# Default-Stop:
# Short-Description: naming services LDAP client daemon
# Description: Provides naming services using a directory server.
### END INIT INFO

program=/usr/sbin/nslcd
prog=${program##*/}
pidfile=/var/run/nslcd/nslcd.pid
rundir=/var/run/nslcd

k5start=/usr/bin/k5start
k5start_pidfile=${rundir}/k5start_nslcd.pid
k5start_user=$(sed -n 's/^uid *\([^ ]*\) *$/\1/ip' /etc/nslcd.conf)
k5start_group=$(sed -n 's/^gid *\([^ ]*\) *$/\1/ip' /etc/nslcd.conf)
k5start_ccfile=$(sed -n 's/^krb5_ccname *\(FILE:\)\?\([^: ]*\) *$/\2/ip' /etc/nslcd.conf)

if [ -f /etc/rc.d/init.d/functions ]; then
	. /etc/rc.d/init.d/functions
fi

[ -f /etc/sysconfig/nslcd ] && . /etc/sysconfig/nslcd

RETVAL=0

start() {
    grep -q '^sasl_mech *GSSAPI$' /etc/nslcd.conf
    if [ $? -eq 0 ]; then
        echo -n $"Starting k5start for nslcd"
        daemon $k5start -b -p $k5start_pidfile -o $k5start_user \
            -g $k5start_group -m 600 -f /etc/krb5.keytab -K 60 \
            -u $K5START_PRINCIPAL -k $k5start_ccfile
        echo
    fi
    echo -n $"Starting $prog: "
    daemon $program
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && touch /var/lock/subsys/$prog
    return $RETVAL
}

stop() {
    if [ $? -eq 0 ]; then
        echo -n $"Stopping k5start for nslcd"
        killproc -p $k5start_pidfile $k5start
        echo
    fi
    echo -n $"Stopping $prog: "
    killproc $program
    RETVAL=$?
    echo
    if [ $RETVAL -eq 0 ]; then
	rm -f /var/lock/subsys/$prog
    fi
}

restart() {
    stop
    start
}

# See how we were called.
case "$1" in
    start)
        [ -f /var/lock/subsys/$prog ] && exit 0
        $1
        ;;
    stop)
        [ -f /var/lock/subsys/$prog ] || exit 0
        $1
        ;;
    restart)
        $1
        ;;
    status)
        status -p $pidfile $program
        RETVAL=$?
        ;;
    condrestart|try-restart)
        [ -f /var/lock/subsys/$prog ] && restart || :
        ;;
    reload)
        echo "can't reload configuration, you have to restart it"
        RETVAL=3
        ;;
    force-reload)
        restart
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart|try-restart|reload|force-reload}"
        exit 1
        ;;
esac
exit $RETVAL
