# Installs Unbound DNS server

class unbound {

  package { 'unbound':
    ensure => installed,
  }

  service { 'unbound':
    ensure => running,
    name   => 'unbound',
    enable => true,
  }

}

