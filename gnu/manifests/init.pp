# Install GCC.
#
# On Linux hosts this will also install headers required for compiling
# kernel modules.
#
class gnu::gcc {

    package { "gcc":
        ensure => installed,
    }

    if $::kernel == Linux {
      case $::operatingsystem {
        'debian', 'ubuntu': {
          package { "kernel-headers":
            ensure => installed,
            name   => "linux-libc-dev",
          }
        }
        default: {
          package { "kernel-headers":
            ensure  => installed,
            require => Package["kernel-devel"],
          }
          package { "kernel-devel":
            ensure => installed,
          }
        }
      }
    }

}


# Install GNU Debugger.
#
class gnu::gdb {

  package { "gdb":
    ensure => installed,
  }

}


# Install GNU make.
#
class gnu::make {

    package { "make":
        ensure => installed,
        name   => $::operatingsystem ? {
            "openbsd" => "gmake",
            default   => "make",
        },
    }

}


# Install GNU tar.
#
class gnu::tar {

    package { "tar":
        ensure => installed,
        name   => $::operatingsystem ? {
            "openbsd" => "gtar",
            default   => "tar",
        },
    }

}


# Install GNU rcs.
#
class gnu::rcs {

    package { "rcs":
        ensure => installed,
        name   => $::operatingsystem ? {
            "openbsd" => "grcs",
            default   => "rcs",
        },
    }

}
