
# Install memcached server.
class memcached::server {

    package { "memcached":
        ensure => installed,
    }

    service { "memcached":
        ensure  => running,
        enable  => true,
        require => Package["memcached"],
    }

}
