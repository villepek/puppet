# Install Node.js
#
class nodejs {

    case $::operatingsystem {
        "centos","redhat","fedora": {
            package { [ "nodejs", "npm" ]:
                ensure => installed,
            }
        }
        default: {
            fail("nodejs not supported on ${::operatingsystem}")
        }
    }

}
