
class telegram::client {

    if $::kernel != "Linux" {
        fail("telegram::client not supported on ${::operatingsystem}")
    }

    file { "/usr/local/src/telegram.tar.xz":
        ensure => present,
        source => "puppet:///files/packages/${telegram_package_latest}",
        mode   => "0644",
        owner  => "root",
        group  => "root",
    }
    util::extract::tar { "/opt/Telegram":
        ensure  => latest,
        source  => "/usr/local/src/telegram.tar.xz",
        strip   => "1",
        require => File["/usr/local/src/telegram.tar.xz"],
    } 

    file { "/usr/local/bin/telegram":
        ensure  => link,
        target  => "/opt/Telegram/Telegram",
        owner   => "root",
        group   => "root",
        require => Util::Extract::Tar["/opt/Telegram"],
    }

}
