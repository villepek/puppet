
# Install lsof
#
class lsof {

  package { "lsof":
    ensure => installed,
  }

}
