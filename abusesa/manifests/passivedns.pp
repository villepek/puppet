# Install abusesa-passivedns.
#
class abusesa::passivedns(
  $datadir='/var/lib/passivedns',
) {

  if ! $abusesa_passivedns_package {
    if $::abusesa_passivedns_package_latest {
      $abusesa_passivedns_package = $::abusesa_passivedns_package_latest
    } else {
      fail('Must define $abusesa_passivedns_package or $abusesa_passivedns_package_latest')
    }
  }

  include user::system
  realize(User['passdns'], Group['passdns'])

  exec { 'usermod-abusesa-passivedns':
    path    => '/bin:/usr/bin:/sbin:/usr/sbin',
    command => 'usermod -a -G passdns abusesa',
    unless  => 'id -n -G abusesa | grep \'\bpassdns\b\'',
    require => [
      User['abusesa'],
      Group['passdns'],
    ],
  }

  if $datadir != '/var/lib/passivedns' {
    file { '/var/lib/passivedns':
      ensure => link,
      target => $datadir,
    }
  }

  file { $datadir:
    ensure => directory,
    mode   => '2770',
    owner  => 'passdns',
    group  => 'passdns',
  }

  file { [
    '/var/lib/passivedns/log',
    '/var/lib/passivedns/storage',
  ]:
    ensure => directory,
    mode   => '2770',
    owner  => 'passdns',
    group  => 'passdns',
  }

  file { '/var/lib/passivedns/.profile':
    ensure  => present,
    mode    => '0600',
    owner   => 'passdns',
    group   => 'passdns',
    content => "umask 007\n",
  }

  python::pip::install { 'abusesa-passivedns.tar.gz':
    source => "puppet:///files/packages/${abusesa_passivedns_package}",
  }

  Python::Pip::Install['abusesa.tar.gz'] ->
  Python::Pip::Install['abusesa-passivedns.tar.gz']

}
