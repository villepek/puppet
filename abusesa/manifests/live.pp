# Install AbuseSA Live.
#
# === Parameters
#
# $authurl:
#   Authentication path. Defaults to '/collab/?action=authcredentials'.
#
# $boshurl:
#   XMPP BOSH path. Defaults to '/bosh/'.
#
# $config:
#   Source URL of custom config file.
#
# $mapurl:
#   Alternate base URL of map tiles.
#
# $colormap:
#   URL of custom color map.
#
# $webhosts:
#   List of live virtual hosts.
#
class abusesa::live(
  $authurl='/collab/?action=authcredentials',
  $boshurl='/bosh/',
  $mapurl=undef,
  $colormap=undef,
  $config=undef,
  $webhosts=undef,
) {

  if ! $abusesa_live_package {
    if $::abusesa_live_package_latest {
      $abusesa_live_package = $::abusesa_live_package_latest
    } else {
      fail('Must define $abusesa_live_package or $abusesa_live_package_latest')
    }
  }

  python::pip::install { 'abusesa-live.tar.gz':
    source => "puppet:///files/packages/${abusesa_live_package}",
  }

  $htdocs = $::operatingsystem ? {
    'ubuntu' => '/usr/local/share/abusesa-live/htdocs',
    default  => '/usr/share/abusesa-live/htdocs',
  }

  if $config {
    $config_content = undef
  } else {
    $config_content = template('abusesa/live/config.json.erb')
  }

  file { "${htdocs}/config.json":
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    source  => $config,
    content => $config_content,
    require => Python::Pip::Install['abusesa-live.tar.gz'],
  }

  if $webhosts {
    apache::configfile { 'vsroom.conf':
      http   => false,
      source => 'puppet:///modules/abusesa/vsroom-httpd.conf',
    }

    abusesa::live::configwebhost { $webhosts:
      htdocs => $htdocs,
    }
  }

}


# Enable AbuseSA Live for virtual host.
#
define abusesa::live::configwebhost($htdocs) {

  if ! defined(Abusesa::Configwebhost[$name]) {
    abusesa::configwebhost { $name: }
  }

  file { "/srv/www/https/${name}/abusesa/live":
    ensure => link,
    target => $htdocs,
  }

  file { "/srv/www/https/${name}/vsroom":
    ensure => directory,
    force  => true,
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }
  file { "/srv/www/https/${name}/vsroom/.htaccess":
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => "RedirectMatch permanent /vsroom/overview/(index\\.html)? /abusesa/live/\n",
  }

}
