# Install AbuseSA Analytics UI.
#
# === Parameters
#
# $config:
#   Source URL of optional config file.
#
# $webhosts:
#   List of analytics virtual hosts.
#
class abusesa::analytics::ui(
  $config=undef,
  $webhosts=undef,
) {

  if ! $abusesa_analytics_ui_package {
    if $::abusesa_analytics_ui_package_latest {
      $abusesa_analytics_ui_package = $::abusesa_analytics_ui_package_latest
    } else {
      fail('Must define $abusesa_analytics_ui_package or $abusesa_analytics_ui_package_latest')
    }
  }

  python::pip::install { 'abusesa-analytics-ui.tar.gz':
    source => "puppet:///files/packages/${abusesa_analytics_ui_package}",
  }

  $htdocs = $::operatingsystem ? {
    'ubuntu' => '/usr/local/share/analytics/htdocs',
    default  => '/usr/share/analytics/htdocs',
  }

  if $config {
    file { "${htdocs}/config.json":
      ensure  => present,
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      source  => $config,
      require => Python::Pip::Install['abusesa-analytics-ui.tar.gz'],
    }
  }

  if $webhosts {
    abusesa::analytics::ui::configwebhost { $webhosts:
      htdocs => $htdocs,
    }
  }

}


# Enable AbuseSA Analytics for virtual host.
#
define abusesa::analytics::ui::configwebhost($htdocs) {

  if ! defined(Abusesa::Configwebhost[$name]) {
    abusesa::configwebhost { $name: }
  }

  file { "/srv/www/https/${name}/abusesa/analytics":
    ensure => link,
    target => $htdocs,
  }

}
