# Install AbuseSA Uploader.
#
# === Parameters
#
# $authurl:
#   Authentication path. Defaults to '/collab/?action=authcredentials'.
#
# $boshurl:
#   XMPP BOSH path. Defaults to '/bosh/'.
#
# $room:
#   Default destination room.
#
# $webhosts:
#   List of uploader virtual hosts.
#
class abusesa::uploader(
  $authurl='/collab/?action=authcredentials',
  $boshurl='/bosh/',
  $room=undef,
  $webhosts=undef,
  $ratelimit='10',
) {

  if ! $abusesa_uploader_package {
    if $::abusesa_uploader_package_latest {
      $abusesa_uploader_package = $::abusesa_uploader_package_latest
    } else {
      fail('Must define $abusesa_uploader_package or $abusesa_uploader_package_latest')
    }
  }

  python::pip::install { 'abusesa-uploader.tar.gz':
    source => "puppet:///files/packages/${abusesa_uploader_package}",
  }

  $htdocs = $::operatingsystem ? {
    'ubuntu' => '/usr/local/share/abusesa-uploader/htdocs',
    default  => '/usr/share/abusesa-uploader/htdocs',
  }

  file { "${htdocs}/config.json":
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('abusesa/uploader/config.json.erb'),
    require => Python::Pip::Install['abusesa-uploader.tar.gz'],
  }

  file { "${htdocs}/.htaccess":
    ensure  => $webhosts ? {
      undef   => present,
      default => absent,
    },
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => "RewriteEngine Off\n",
    require => Python::Pip::Install['abusesa-uploader.tar.gz'],
  }

  if $webhosts {
    abusesa::uploader::configwebhost { $webhosts:
      htdocs => $htdocs,
    }
  }

}


# Enable AbuseSA Uploader for virtual host.
#
define abusesa::uploader::configwebhost($htdocs) {

  if ! defined(Abusesa::Configwebhost[$name]) {
    abusesa::configwebhost { $name: }
  }

  file { "/srv/www/https/${name}/abusesa/uploader":
    ensure => link,
    target => $htdocs,
  }

}
