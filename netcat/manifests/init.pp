
# Install netcat
#
class netcat {

    if $::operatingsystem != "OpenBSD" {
        package { "netcat":
            ensure => present,
            name   => $::operatingsystem ? {
                "debian" => "netcat",
                "ubuntu" => "netcat",
                "centos" => $::operatingsystemrelease ? {
                    /^[1-6]/ => "nc",
                    default  => "nmap-ncat",
                },
                "fedora" => $::operatingsystemrelease ? {
                    /^1[0-7]/ => "nc",
                    default   => "nmap-ncat",
                },
                default  => "nc",
            },
        }
    }

}
