# Install OpenJDK development environment.
#
class openjdk::jdk($version='7') {

  case $::operatingsystem {
    'centos','redhat': {
      package { "java-1.${version}.0-openjdk-devel":
        ensure => installed,
      }
    }
    'ubuntu': {
      package { "openjdk-${version}-jdk":
        ensure => installed,
      }
    }
    default: {
      fail("openjdk not supported on ${::operatingsystem}")
    }
  }

}


# Install OpenJDK runtime environment.
#
class openjdk::jre($version='7') {

  case $::operatingsystem {
    'centos','redhat': {
      package { "java-1.${version}.0-openjdk":
        ensure => installed,
      }
    }
    'ubuntu': {
      package { "openjdk-${version}-jre":
        ensure => installed,
      }
    }
    default: {
      fail("openjdk not supported on ${::operatingsystem}")
    }
  }

}
