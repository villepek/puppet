# Install NFS client.
#
class nfs::client {

    require portmap::server

    package { "nfs-utils":
        ensure => installed,
        name   => $::operatingsystem ? {
            "ubuntu" => "nfs-common",
            default  => "nfs-utils",
        },
    }

    case $::operatingsystem {
        "centos", "redhat": {
            if versioncmp($::operatingsystemrelease, "7") < 0 {
                $service = "nfslock"
            } else {
                $service = "rpc-statd"
            }
        }
        "fedora": {
            # https://bugzilla.redhat.com/show_bug.cgi?id=692008
            service { "NetworkManager-wait-online":
                ensure => stopped,
                enable => true,
            }
            if versioncmp($::operatingsystemrelease, "16") < 0 {
                $service = "nfslock"
            } else {
                $service = "rpc-statd"
            }
        }
        "ubuntu": {
            file { "/etc/modprobe.d/lockd.conf":
                ensure  => present,
                mode    => "0644",
                owner   => "root",
                group   => "root",
                content => "options lockd nlm_tcpport=4001 nlm_udpport=4001\n",
                before  => Package["nfs-utils"],
            }
            augeas { "set-nfs-common":
                context => "/files/etc/default/nfs-common",
                changes => "set STATDOPTS '\"--port 4000\"'",
                notify  => Service["nfslock"],
                require => Package["nfs-utils"],
            }
            if versioncmp($::operatingsystemrelease, "16.04") < 0 {
                $service = "statd"
            } else {
                $service = "rpc-statd"
            }
        }
        "debian": {
            $service = "statd"
        }
        default: {
            $service = "nfslock"
        }
    }

    service { "nfslock":
        ensure    => running,
        enable    => true,
        name      => $service,
        hasstatus => true,
        require   => Package["nfs-utils"],
    }

}


# Install and configure NFS server.
#
# === Parameters
#
#   $servers:
#       Number of nfs server processes to be started. Defaults to 8.
#
#   $versions:
#       Array of NFS versions to enable. Defaults to [ "3" ].
#
class nfs::server(
    $servers="8",
    $versions=["3"],
) {

    $default_versions = [ "2", "3", "4" ]
    $disable_versions = inline_template('<%= (@default_versions - @versions).map { |v| "-N %s" % v }.join(" ") %>')

    require nfs::client

    file { "/etc/exports":
        ensure => present,
        source => [
          "puppet:///files/nfs/exports.${::homename}",
          "puppet:///modules/nfs/exports",
        ],
        mode   => "0644",
        owner  => "root",
        group  => "root",
        notify => Exec["exportfs"],
    }

    case $::operatingsystem {
        "centos","redhat","fedora": {
            file { "/etc/sysconfig/nfs":
                ensure  => present,
                mode    => "0644",
                owner   => "root",
                group   => "root",
                content => template("nfs/nfs.sysconfig.erb"),
                notify  => Service["nfs"],
            }
        }
        "ubuntu": {
            package { "nfs-kernel-server":
                ensure => installed,
            }
            file { "/etc/default/nfs-kernel-server":
                ensure  => present,
                mode    => "0644",
                owner   => "root",
                group   => "root",
                content => template("nfs/nfs-kernel-server.erb"),
                require => Package["nfs-kernel-server"],
                notify  => Service["nfs"],
            }
        }
        default: { }
    }

    service { "nfs":
        ensure    => running,
        name      => $::operatingsystem ? {
            "centos" => $::operatingsystemrelease ? {
                /^[1-6]/ => "nfs",
                default  => "nfs-server",
            },
            "fedora" => $::operatingsystemrelease ? {
                /^([1-9]|1[0-5])$/ => "nfs",
                default            => "nfs-server",
            },
            "ubuntu" => "nfs-kernel-server",
            default  => "nfs",
        },
        enable    => true,
        hasstatus => true,
    }

    exec { "exportfs":
        command     => "exportfs -rav",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
        require     => Service["nfs"],
    }

}
