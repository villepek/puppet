
# Install Spotify
#
class spotify {

    if $::operatingsystem != "Fedora" {
        fail("spotify not supported in ${::operatingsystem}")
    }

    yum::repo { "spotify":
        descr   => "Spotify digital music service",
        baseurl => "http://negativo17.org/repos/spotify/fedora-\$releasever/\$ba
search/",
        gpgkey  => "http://negativo17.org/repos/RPM-GPG-KEY-slaanesh",
    }
    package { "spotify-client":
        ensure  => installed,
        require => Yum::Repo["spotify"],
    }

}
